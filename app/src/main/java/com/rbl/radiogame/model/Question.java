package com.rbl.radiogame.model;

import android.content.Context;

import com.rbl.radiogame.model.enums.QuestionAlternativeViewMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

public class Question {

    private int questionTitleId;
    private int questionDrawableId;
    private List<QuestionAlternative> questionAlternatives;
    private UUID chosenAlternativeId;
    private QuestionAlternative correctAlternative;
    private int correctAlternativeHintStringId;
    private @QuestionAlternativeViewMode.QuestionAlternativeViewModeTypeDef
    int viewMode;

    public Question(int questionTitleId,
                    int questionDrawableId,
                    @QuestionAlternativeViewMode.QuestionAlternativeViewModeTypeDef int viewMode) {
        this.questionTitleId = questionTitleId;
        this.questionDrawableId = questionDrawableId;
        this.viewMode = viewMode;
    }

    public int getQuestionTitleId() {
        return questionTitleId;
    }

    public int getQuestionDrawableId() {
        return questionDrawableId;
    }

    public UUID getChosenAlternativeId() {
        return chosenAlternativeId;
    }

    public List<QuestionAlternative> getQuestionAlternatives() {
        return questionAlternatives;
    }

    public QuestionAlternative getCorrectAlternative() {
        return correctAlternative;
    }

    public int getCorrectAlternativeHintStringId() {
        return correctAlternativeHintStringId;
    }

    public @QuestionAlternativeViewMode.QuestionAlternativeViewModeTypeDef
    int getViewMode() {
        return viewMode;
    }

    public void addQuestionAlternative(QuestionAlternative questionAlternative) {
        if (questionAlternatives == null) {
            questionAlternatives = new ArrayList<>();
        }

        if (questionAlternative != null) {
            this.questionAlternatives.add(questionAlternative);
        }
    }

    public void setQuestionAlternatives(List<QuestionAlternative> questionAlternatives) {
        this.questionAlternatives = questionAlternatives;
    }

    public void setCorrectAlternative(QuestionAlternative correctAlternative) {
        this.correctAlternative = correctAlternative;
    }

    public void setCorrectAlternativeHintStringId(int correctAlternativeHintStringId) {
        this.correctAlternativeHintStringId = correctAlternativeHintStringId;
    }

    public boolean answerQuestion(UUID questionAlternativeId, boolean validateAlternativeId) throws Exception {
        if (!validateAlternativeId) {
            this.chosenAlternativeId = questionAlternativeId;
            return hasBeenAnsweredCorrectly();
        } else {
            QuestionAlternative questionAlternative = getAlternative(questionAlternativeId);
            if (questionAlternative != null) {
                this.chosenAlternativeId = questionAlternativeId;
                questionAlternative.setUserAnswer(true);
                return hasBeenAnsweredCorrectly();
            } else {
                throw new Exception("Alternative not found!");
            }
        }
    }

    private QuestionAlternative getAlternative(UUID questionAlternativeId) throws Exception {
        if (isValidQuestion()) {
            if (questionAlternativeId != null) {
                for (QuestionAlternative questionAlternative : questionAlternatives) {
                    if (questionAlternative.getId().toString().equals(questionAlternativeId.toString())) {
                        return questionAlternative;
                    }
                }
                return null;
            } else {
                return null;
            }
        } else {
            throw new Exception("Question is invalid!");
        }
    }

    public boolean isValidQuestion() {
        return (questionTitleId > 0 || questionDrawableId > 0)
                && correctAlternative != null
                && questionAlternatives != null
                && questionAlternatives.size() > 0;
    }

    public void shuffleAlternatives(Context ctx) {
        if (isValidQuestion()) {
            Collections.shuffle(this.questionAlternatives);

            int alternativeCount = 0;
            for (QuestionAlternative alternative : this.questionAlternatives) {
                alternativeCount++;
                alternative.setAlternativeLetterId(ctx.getResources()
                        .getIdentifier("alternative_" + alternativeCount,
                                "string", ctx.getPackageName()));
            }
        }
    }

    public boolean hasBeenAnswered() {
        return chosenAlternativeId != null;
    }

    private boolean hasBeenAnsweredCorrectly() {
        if (hasBeenAnswered()) {
            return chosenAlternativeId.toString().equals(correctAlternative.getId().toString());
        }
        return false;
    }
}
