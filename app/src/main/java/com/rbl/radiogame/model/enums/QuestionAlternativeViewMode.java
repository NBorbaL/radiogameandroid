package com.rbl.radiogame.model.enums;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class QuestionAlternativeViewMode {
    // ... type definitions
    // Describes when the annotation will be discarded
    @Retention(RetentionPolicy.SOURCE)
    // Enumerate valid values for this interface
    @IntDef({GRID, LIST})
    // Create an interface for validating int types
    public @interface QuestionAlternativeViewModeTypeDef {
    }

    // Declare the constants
    public static final int GRID = 0;
    public static final int LIST = 1;

    public static final int GRID_DEFAULT_COLUMN_WIDTH_DP = 180;

}
