package com.rbl.radiogame.model.json;

public class JsonAlternative
{
    private String drawable;

    private boolean correct;

    private boolean shouldShowDescription;

    private String title;

    public String getDrawable ()
    {
        return drawable;
    }

    public void setDrawable (String drawable)
    {
        this.drawable = drawable;
    }

    public boolean isCorrect ()
    {
        return correct;
    }

    public void setCorrect (boolean correct)
    {
        this.correct = correct;
    }

    public boolean getShouldShowDescription ()
    {
        return shouldShowDescription;
    }

    public void setShouldShowDescription (boolean shouldShowDescription)
    {
        this.shouldShowDescription = shouldShowDescription;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [drawable = "+drawable+", correct = "+correct+", shouldShowDescription = "+shouldShowDescription+", title = "+title+"]";
    }
}