package com.rbl.radiogame.model;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;

import com.rbl.radiogame.dal.QuestionDatabase;
import com.rbl.radiogame.defaults.GameDefaults;
import com.rbl.radiogame.interfaces.IGameClientCallback;
import com.rbl.radiogame.interfaces.OnGameChangeCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Game {

    private QuestionDatabase questionDatabase;
    private boolean isGameStarted = false;

    private List<Question> gameQuestions;
    private int currentQuestionInt = 0;
    private Question currentQuestion;

    private OnGameChangeCallback onGameChangeCallback;
    private CountDownTimer questionTimer;

    private int correctAnswerQuantity = 0;
    private int incorrectAnswerQuantity = 0;

    public Game(Context ctx, OnGameChangeCallback onGameChangeCallback) throws Exception {
        this.onGameChangeCallback = onGameChangeCallback;
        questionDatabase = new QuestionDatabase(ctx);
        setQuestionQuantity(GameDefaults.DEFAULT_QUESTION_QUANTITY);
    }

    public void setQuestionQuantity(int questionQuantity) throws Exception {
        if (!isGameStarted) {
            this.gameQuestions = questionDatabase.getGameQuestions(questionQuantity);
        } else {
            throw new Exception("Question list cannot be changed after has started!");
        }
    }

    public boolean isGameStarted() {
        return isGameStarted;
    }

    public List<Question> getCurrentGameQuestions() {
        return gameQuestions;
    }

    public Question getCurrentQuestion() {
        return currentQuestion;
    }

    public void nextQuestion() throws Exception {
        if (isGameStarted()) {
            boolean hasNextQuestion = false;
            for (Question question : gameQuestions) {
                if (!question.hasBeenAnswered()) {
                    currentQuestion = question;
                    currentQuestionInt++;
                    onGameChangeCallback.onQuestionShow(currentQuestionInt, gameQuestions.size(),
                            currentQuestion, new IGameClientCallback() {
                                @Override
                                public void canStartQuestionTimer() {
                                    startQuestionTimer();
                                }
                            });
                    hasNextQuestion = true;
                    break;
                }
            }

            if (!hasNextQuestion) {
                currentQuestionInt = 0;
                currentQuestion = null;
                endGame();
            }
        } else {
            throw new Exception("Game has not started yet!");
        }
    }

    public void startGame(int questionQuantity) throws Exception {
        try {
            if (onGameChangeCallback != null) {
                // Setup game database
                setQuestionQuantity(questionQuantity > 0 ? questionQuantity : GameDefaults.DEFAULT_QUESTION_QUANTITY);
                // Can only start game with enough questions
                if (gameQuestions != null && gameQuestions.size() > 0) {
                    isGameStarted = true;
                    onGameChangeCallback.onGameStart();
                } else {
                    throw new IllegalArgumentException("Game has not been supplied questions");
                }
            } else {
                throw new IllegalArgumentException("Game callback has not been initialized!");
            }
        } catch (Exception e) {
            throw new Exception("Cannot start game. " + e.getMessage());
        }
    }

    private void startQuestionTimer() {
        if (GameDefaults.DEFAULT_IS_TIMER_ACTIVE) {
            onGameChangeCallback.onQuestionTimerTick(GameDefaults.DEFAULT_QUESTION_TIME_MILLIS / 1000);
            questionTimer = new CountDownTimer(GameDefaults.DEFAULT_QUESTION_TIME_MILLIS, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    onGameChangeCallback.onQuestionTimerTick(millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    onGameChangeCallback.onQuestionTimerTick(0);
                    try {
                        // Generate incorrect UUID for incorrect answer
                        answerQuestion(UUID.randomUUID(), false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

    public void answerQuestion(UUID questionAlternativeId) throws Exception {
        answerQuestion(questionAlternativeId, true);
    }

    private void answerQuestion(UUID questionAlternativeId, boolean validateAlternativeId) throws Exception {
        if (isGameStarted) {
            if (currentQuestion != null) {
                // Stop question timer
                if (questionTimer != null) {
                    questionTimer.cancel();
                }
                boolean correctAnswer = currentQuestion.answerQuestion(questionAlternativeId,
                        validateAlternativeId);
                if (correctAnswer) {
                    correctAnswerQuantity++;
                } else {
                    incorrectAnswerQuantity++;
                }
                onGameChangeCallback.onQuestionAnswered(correctAnswer);
            } else {
                throw new Exception("Game has not started yet!");
            }
        }
    }

    public void showCurrentQuestionHints(boolean showHint, boolean showDescription) {
        if (currentQuestion != null) {
            for (QuestionAlternative alternative : currentQuestion.getQuestionAlternatives()) {
                if (alternative != null) {
                    alternative.setClickable(!showHint);
                    alternative.setShouldShowHint(showHint);
                    alternative.setShouldShowDescription(showDescription);
                }
            }
            onGameChangeCallback.onQuestionAlternativesUpdated(currentQuestion);
        }
    }

    public void endGame() throws Exception {
        if (isGameStarted) {
            isGameStarted = false;
            int totalQuestions = correctAnswerQuantity + incorrectAnswerQuantity;
            float correctAnswerPercentage = (((float)correctAnswerQuantity / (float)totalQuestions) * 100);
            boolean isWin = correctAnswerPercentage >= GameDefaults.DEFAULT_PERCENT_FOR_WIN;
            onGameChangeCallback.onGameEnd(isWin, correctAnswerQuantity,
                    incorrectAnswerQuantity, correctAnswerPercentage);
            resetAllGameParameters();
        } else {
            throw new Exception("Game has not been started yet!");
        }
    }

    private void resetAllGameParameters() {
        correctAnswerQuantity = 0;
        incorrectAnswerQuantity = 0;
        currentQuestion = null;
        currentQuestionInt = 0;
    }
}
