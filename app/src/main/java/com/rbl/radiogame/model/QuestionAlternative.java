package com.rbl.radiogame.model;

import com.rbl.radiogame.R;
import com.rbl.radiogame.defaults.GameDefaults;

import java.util.UUID;

public class QuestionAlternative {

    private UUID id;
    private int alternativeLetterId;
    private int alternativeDescriptionId;
    private int alternativeDrawableId;
    private int alternativeHintId;
    private int alternativeHintBackgroundColorId;
    private boolean shouldShowHint;
    private boolean shouldShowDescription;
    private boolean isClickable = true;
    private boolean isUserAnswer = false;

    public QuestionAlternative(int alternativeLetterId, int alternativeDescriptionId, int alternativeDrawableId,
                               int alternativeHintId, boolean shouldShowDescription) {
        this.id = UUID.randomUUID();
        this.alternativeLetterId = alternativeLetterId;
        this.alternativeDescriptionId = alternativeDescriptionId;
        this.alternativeDrawableId = alternativeDrawableId;
        this.alternativeHintId = alternativeHintId;
        this.alternativeHintBackgroundColorId = GameDefaults.DEFAULT_ERROR_HINT_BACKGROUND_COLOR_ID;
        this.shouldShowDescription = shouldShowDescription;
        this.shouldShowHint = false;
    }

    public UUID getId() {
        return id;
    }

    public int getAlternativeLetterId() {
        return alternativeLetterId;
    }

    public void setAlternativeLetterId(int alternativeLetterId) {
        this.alternativeLetterId = alternativeLetterId;
    }

    public int getAlternativeDescriptionId() {
        return alternativeDescriptionId;
    }

    public int getAlternativeDrawableId() {
        return alternativeDrawableId;
    }

    public int getAlternativeHintId() {
        return alternativeHintId;
    }

    public int getAlternativeHintBackgroundColorId() {
        return alternativeHintBackgroundColorId;
    }

    public void setAlternativeHintBackgroundColorId(int alternativeHintBackgroundColorId) {
        if (alternativeHintBackgroundColorId > 0) {
            this.alternativeHintBackgroundColorId = alternativeHintBackgroundColorId;
        }
    }

    public boolean isShouldShowDescription() {
        return shouldShowDescription;
    }

    public void setShouldShowDescription(boolean shouldShowDescription) {
        this.shouldShowDescription = shouldShowDescription;
    }

    public boolean isShouldShowHint() {
        return shouldShowHint;
    }

    public void setShouldShowHint(boolean shouldShowHint) {
        this.shouldShowHint = shouldShowHint;
    }

    public boolean isClickable() {
        return isClickable;
    }

    public void setClickable(boolean clickable) {
        isClickable = clickable;
    }

    public boolean isUserAnswer() {
        return isUserAnswer;
    }

    public void setUserAnswer(boolean userAnswer) {
        isUserAnswer = userAnswer;
    }
}
