package com.rbl.radiogame.model.json;

import java.util.List;

public class JsonQuestion {
    private String drawable;

    private List<JsonAlternative> alternatives;

    private String title;

    private int viewMode;

    public String getDrawable() {
        return drawable;
    }

    public void setDrawable(String drawable) {
        this.drawable = drawable;
    }

    public List<JsonAlternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<JsonAlternative> alternatives) {
        this.alternatives = alternatives;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getViewMode() {
        return viewMode;
    }

    public void setViewMode(int viewMode) {
        this.viewMode = viewMode;
    }

    @Override
    public String toString() {
        return "ClassPojo [drawable = " + drawable + ", alternatives = " + alternatives + ", title = " + title + ", viewMode = " + viewMode + "]";
    }
}