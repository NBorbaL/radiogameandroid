package com.rbl.radiogame.model.json;

import java.util.List;

public class JsonQuestionFile
{
    private List<JsonQuestion> questions;

    public List<JsonQuestion> getQuestions ()
    {
        return questions;
    }

    public void setQuestions (List<JsonQuestion> questions)
    {
        this.questions = questions;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [questions = "+questions+"]";
    }
}