package com.rbl.radiogame.util;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;

public class StringUtil {
    public static Spanned returnFormattedString(Context ctx, int resId, Object... formatArgs) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(ctx.getString(resId, formatArgs), Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(ctx.getString(resId, formatArgs));
        }
    }

    public static String rtrim(String toTrim) {
        char[] val = toTrim.toCharArray();
        int len = val.length;

        while (len > 0 && val[len - 1] <= ' ') {
            len--;
        }
        return len < val.length ? toTrim.substring(0, len) : toTrim;
    }

    public static String ltrim(String toTrim) {
        int st = 0;
        char[] val = toTrim.toCharArray();
        int len = val.length;

        while (st < len && val[st] <= ' ') {
            st++;
        }
        return st > 0 ? toTrim.substring(st, len) : toTrim;
    }

    public static String trimEnd(String value) {
        int len = value.length();
        int st = 0;
        while ((st < len) && Character.isWhitespace(value.charAt(len - 1))) {
            len--;
        }
        return value.substring(0, len);
    }
}
