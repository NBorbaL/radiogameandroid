package com.rbl.radiogame.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Criado por renan.lucas em 21/03/2017.
 */

public class ViewUtil {

    /**
     * Mostra teclado
     *
     * @param activity Instância da atividade que contém a view do teclado
     */
    public static void mostrarTeclado(Activity activity, EditText editText) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    /**
     * Mostra teclado
     *
     * @param activity Instância da atividade que contém a view do teclado
     */
    public static void mostrarTeclado(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        // Acha a view que está com foco
        View view = activity.getCurrentFocus();

        // Se nenhuma view tem foco, cria uma nova para poder esconder o teclado
        if (view == null) {
            view = new View(activity);
        }

        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    /**
     * Verifica se na atividade passada via parâmetro exista alguma view com foco
     * e esconde o teclado
     *
     * @param activity Instância da atividade que contém a view do teclado
     */
    public static void esconderTeclado(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        // Acha a view que está com foco
        View view = activity.getCurrentFocus();

        // Se nenhuma view tem foco, cria uma nova para poder esconder o teclado
        if (view == null) {
            view = new View(activity);
        }

        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Verifica se na atividade passada via parâmetro exista alguma view com foco
     * e esconde o teclado
     *
     * @param activity Instância da atividade que contém a view do teclado
     */
    public static void esconderTeclado(Activity activity, View view) {
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static void disableKeyboardForEditText(@NonNull EditText editText) {
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    public static void enableKeyboardForEditText(@NonNull EditText editText) {
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }

    /**
     * Cria e mostra um alert customizado
     *
     * @param ctx           Contexto em que o aplicativo se encontra
     * @param titulo        Titulo do alerta
     * @param mensagem      Mensagem do alerta
     * @param mensagemBotao Mensagem do botão do alerta
     * @param cancelavel    Indica se o alerta é cancelavel ou não
     */
    public static void criaEMostraAlert(Context ctx, String titulo, String mensagem, String mensagemBotao, boolean cancelavel) {
        if (ctx != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setTitle(titulo);
            builder.setMessage(mensagem);
            builder.setCancelable(cancelavel);

            builder.setPositiveButton(
                    mensagemBotao,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alerta = builder.create();
            alerta.show();
        }
    }

    /**
     * Cria e mostra um alert customizado
     *
     * @param titulo        Titulo do alerta
     * @param mensagem      Mensagem do alerta
     * @param mensagemBotao Mensagem do botão do alerta
     * @param cancelavel    Indica se o alerta é cancelavel ou não
     */
    public static AlertDialog criaEMostraAlertMainThread(final Activity activity, final String titulo, final String mensagem, final String mensagemBotao,
                                                         final boolean cancelavel, final Runnable executarAposFechar) {
        if (activity != null) {
            FutureTask<AlertDialog> task = new FutureTask<AlertDialog>(new Callable<AlertDialog>() {
                @Override
                public AlertDialog call() throws Exception {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(titulo);
                    builder.setMessage(mensagem);
                    builder.setCancelable(cancelavel);

                    builder.setPositiveButton(
                            mensagemBotao,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    executarAposFechar.run();
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alerta = builder.create();
                    alerta.show();
                    return alerta;
                }
            });

            activity.runOnUiThread(task);
            try {
                return task.get();
            } catch (ExecutionException e) {
                return null;
            } catch (InterruptedException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * Cria e mostra um alert customizado
     *
     * @param titulo        Titulo do alerta
     * @param mensagem      Mensagem do alerta
     * @param mensagemBotao Mensagem do botão do alerta
     * @param cancelavel    Indica se o alerta é cancelavel ou não
     */
    public static void criaEMostraAlertMainThread(final Activity activity, final String titulo, final String mensagem, final String mensagemBotao, final boolean cancelavel) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(titulo);
                    builder.setMessage(mensagem);
                    builder.setCancelable(cancelavel);

                    builder.setPositiveButton(
                            mensagemBotao,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alerta = builder.create();
                    alerta.show();
                }
            });
        }
    }

    /**
     * Recebe um boolean que indica a ação que o usuário quer exercer sobre o Dialog. <br/>
     * <b>False</b> - Dismiss <br/>
     * <b>True</b> - Mostrar
     *
     * @param visivel Ação que o usuário quer exercer sobre o dialog
     */
    public static void VisibilidadeDialog(boolean visivel, Context ctx, ProgressDialog dialog, int idString) {
        if (!visivel) {
            // Esconde dialog
            dialog.dismiss();
        } else {
            // Mostrar dialog
            dialog.setMessage(ctx.getString(idString));
            dialog.show();
        }
    }

    /**
     * Recebe um boolean que indica a ação que o usuário quer exercer sobre o Dialog. <br/>
     * <b>False</b> - Dismiss <br/>
     * <b>True</b> - Mostrar
     *
     * @param visivel Ação que o usuário quer exercer sobre o dialog
     */
    public static void VisibilidadeDialogMainThread(final boolean visivel, final Activity activity, final ProgressDialog dialog, final int idString) {
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!visivel) {
                        // Esconde dialog
                        dialog.dismiss();
                    } else {
                        // Mostrar dialog
                        dialog.setMessage(activity.getString(idString));
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        dialog.show();
                    }
                }
            });
        }
    }

    public static void defineAlturaListaBaseadoNumCampos(ListView lista) {
        ListAdapter adaptadorLista = lista.getAdapter();
        if (adaptadorLista == null) {
            // retorna null
            return;
        }

        // Define o adaptador da lista para pegar o tamanho final da lista
        int alturaTotal = 0;
        for (int i = 0; i < adaptadorLista.getCount(); i++) {
            View itemLista = adaptadorLista.getView(i, null, lista);
            itemLista.measure(0, 0);
            alturaTotal += itemLista.getMeasuredHeight();
        }

        // Define a nova altura nos parametros da lista no adaptador
        ViewGroup.LayoutParams params = lista.getLayoutParams();
        params.height = alturaTotal + (lista.getDividerHeight() * (adaptadorLista.getCount() - 1));
        lista.setLayoutParams(params);
    }

    public static int calculateNoOfColumns(Context context, float columnWidthDp) { // For example columnWidthdp=180
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidthDp = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (screenWidthDp / columnWidthDp + 0.5); // +0.5 for correct rounding to int.
    }

    public static int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int spToPx(float sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    public static int dpToSp(float dp, Context context) {
        return (int) (dpToPx(dp, context) / context.getResources().getDisplayMetrics().scaledDensity);
    }
}