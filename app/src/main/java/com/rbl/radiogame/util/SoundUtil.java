package com.rbl.radiogame.util;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by CWBCode on 23/03/2018.
 */

public class SoundUtil {

    private static Set<MediaPlayer> playersAtivos = new HashSet<MediaPlayer>();

    /**
     * Toca o audio especificado
     *
     * @param ctx Contexto
     * @param id  Id do audio
     */
    public static void tocarAudio(Context ctx, int id) {
        final MediaPlayer mp = MediaPlayer.create(ctx, id);
        MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
                playersAtivos.remove(mp);
            }
        };

        playersAtivos.add(mp);
        mp.setOnCompletionListener(onCompletionListener);
        mp.start();
    }

    /**
     * Toca o audio especificado
     *
     * @param ctx Contexto
     * @param id  Id do audio
     */
    public static void tocarAudio(Context ctx, int id, float volume) {
        final MediaPlayer mp = MediaPlayer.create(ctx, id);
        mp.setVolume(volume, volume);
        MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
                playersAtivos.remove(mp);
            }
        };

        playersAtivos.add(mp);
        mp.setOnCompletionListener(onCompletionListener);
        mp.start();
    }

    public static void tocarAudioSoundPool(Context ctx, int id) {
        SoundPool sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        int soundId = sp.load(ctx, id, 1); // in 2nd param u have to pass your desire ringtone
        sp.play(soundId, 1, 1, 0, 0, 1);
    }
}
