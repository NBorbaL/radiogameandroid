package com.rbl.radiogame.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rbl.radiogame.R;
import com.rbl.radiogame.defaults.GameDefaults;
import com.rbl.radiogame.util.StringUtil;

public class EndGameActivity extends AppCompatActivity {

    public static final String EXTRA_IS_WIN = "is_win";
    public static final String EXTRA_CORRECT_ANSWER_QUANTITY = "correct_answer_quantity";
    public static final String EXTRA_TOTAL_QUESTION_QUANTITY = "total_question_quantity";
    public static final String EXTRA_CORRECT_ANSWER_PERCENTAGE = "correct_answer_percentage";

    private LinearLayout linearLayoutMain;
    private ImageView imageViewStatus;
    private TextView textViewTitle, textViewCorrectAnswers,
            textViewCorrectAnswersPercent, textViewTotalAnswers;
    private Button buttonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);
        findViewsById();
        setClickListeners();
        showEndGameInfo();
    }

    @Override
    public void onBackPressed() {
        buttonBack.callOnClick();
    }

    private void findViewsById() {
        linearLayoutMain = findViewById(R.id.EndGameActivity_LinearMain);
        imageViewStatus = findViewById(R.id.EndGameActivity_ImageViewStatus);
        textViewTitle = findViewById(R.id.EndGameActivity_TextViewTitle);
        textViewCorrectAnswers = findViewById(R.id.EndGameActivity_TextViewCorrectAnswers);
        textViewCorrectAnswersPercent = findViewById(R.id.EndGameActivity_TextViewPercent);
        textViewTotalAnswers = findViewById(R.id.EndGameActivity_TextViewTotalAnswers);
        buttonBack = findViewById(R.id.EndGameActivity_ButtonBack);
    }

    private void setClickListeners() {
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EndGameActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    private void showEndGameInfo() {
        Intent intent = getIntent();
        if (getIntent() != null) {
            int correctAnswers = intent.getIntExtra(EXTRA_CORRECT_ANSWER_QUANTITY, 0);
            int totalQuestionQuantity = intent.getIntExtra(EXTRA_TOTAL_QUESTION_QUANTITY, 0);
            float percent = intent.getFloatExtra(EXTRA_CORRECT_ANSWER_PERCENTAGE, 0);
            boolean isWin = intent.getBooleanExtra(EXTRA_IS_WIN, false);
            if (isWin) {
                textViewTitle.setText(R.string.game_end_win_title);
                imageViewStatus.setImageResource(R.drawable.ic_check_green);
            } else {
                textViewTitle.setText(R.string.game_end_loss_title);
                imageViewStatus.setImageResource(R.drawable.ic_close_red);
            }
            textViewCorrectAnswers.setText(StringUtil.returnFormattedString(this,
                    R.string.game_end_correct_answers_placeholder, correctAnswers));
            textViewCorrectAnswersPercent.setText(StringUtil.returnFormattedString(this,
                    R.string.game_end_correct_answers_percent, (int)percent));
            textViewTotalAnswers.setText(StringUtil.returnFormattedString(this,
                    R.string.game_end_total_answers_placeholder, totalQuestionQuantity));

            imageViewStatus.animate().scaleX(1.3f).scaleY(1.3f).setDuration(300).withEndAction(new Runnable() {
                @Override
                public void run() {
                    imageViewStatus.animate().scaleX(1).scaleY(1).setDuration(300);
                }
            });
            textViewTitle.animate().scaleX(1.3f).scaleY(1.3f).setDuration(300).withEndAction(new Runnable() {
                @Override
                public void run() {
                    textViewTitle.animate().scaleX(1).scaleY(1).setDuration(300);
                }
            });
            textViewCorrectAnswers.animate().scaleX(1.3f).scaleY(1.3f).setDuration(300).withEndAction(new Runnable() {
                @Override
                public void run() {
                    textViewCorrectAnswers.animate().scaleX(1).scaleY(1).setDuration(300);
                }
            });
            textViewCorrectAnswersPercent.animate().scaleX(1.3f).scaleY(1.3f).setDuration(300).withEndAction(new Runnable() {
                @Override
                public void run() {
                    textViewCorrectAnswersPercent.animate().scaleX(1).scaleY(1).setDuration(300);
                }
            });
            textViewTotalAnswers.animate().scaleX(1.3f).scaleY(1.3f).setDuration(300).withEndAction(new Runnable() {
                @Override
                public void run() {
                    textViewTotalAnswers.animate().scaleX(1).scaleY(1).setDuration(300);
                }
            });
        } else {
            buttonBack.callOnClick();
        }
    }
}
