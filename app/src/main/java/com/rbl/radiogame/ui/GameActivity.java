package com.rbl.radiogame.ui;

import android.animation.LayoutTransition;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rbl.radiogame.R;
import com.rbl.radiogame.adapter.QuestionAlternativeAdapter;
import com.rbl.radiogame.components.GridSpacingItemDecoration;
import com.rbl.radiogame.defaults.GameDefaults;
import com.rbl.radiogame.interfaces.IGameClientCallback;
import com.rbl.radiogame.interfaces.OnGameChangeCallback;
import com.rbl.radiogame.model.Game;
import com.rbl.radiogame.model.Question;
import com.rbl.radiogame.model.QuestionAlternative;
import com.rbl.radiogame.model.enums.QuestionAlternativeViewMode;
import com.rbl.radiogame.util.ImageUtil;
import com.rbl.radiogame.util.StringUtil;
import com.rbl.radiogame.util.ViewUtil;

import java.util.ArrayList;

import static android.content.Intent.FLAG_ACTIVITY_NO_HISTORY;

public class GameActivity extends AppCompatActivity {

    public static final int EXTRA_QUESTION_QUANTITY = R.string.extra_question_quantity;
    private int questionQuantity = -1;

    private Game gameInstance;
    private QuestionAlternativeAdapter questionAlternativeAdapter;

    private LinearLayout linearLayoutMain, linearLayoutQuestionInfo;
    private ImageView imageViewBack;
    private CardView cardViewQuestionTimer;
    private RelativeLayout relativeQuestionAlternatives, relativeQuestionButton;
    private TextView textViewQuestionNumber, textViewQuestionTimer, questionTitle;
    private RoundedImageView questionImage;
    private Button btnConfirm, btnNext;
    private RecyclerView recyclerViewAlternatives;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        findViewsById();
        setProperLayoutAnimation();
        getIntentInfo();
        setClickListeners();
        setupGame();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.game_confirm));
        builder.setMessage(getString(R.string.game_quit_question));

        builder.setPositiveButton(getString(R.string.button_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.finish();
            }
        });

        builder.setNegativeButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void getIntentInfo() {
        Intent intent = getIntent();
        if (intent != null) {
            questionQuantity = intent.getIntExtra(getString(EXTRA_QUESTION_QUANTITY),
                    GameDefaults.DEFAULT_QUESTION_QUANTITY);
        }
    }

    private void findViewsById() {
        linearLayoutMain = findViewById(R.id.GameActivity_LinearMain);
        imageViewBack = findViewById(R.id.GameActivity_ImageViewBack);
        textViewQuestionNumber = findViewById(R.id.GameActivity_TextViewQuestionNumber);
        linearLayoutQuestionInfo = findViewById(R.id.GameActivity_LinearQuestionInfo);
        cardViewQuestionTimer = findViewById(R.id.GameActivity_CardViewQuestionTimer);
        textViewQuestionTimer = findViewById(R.id.GameActivity_TextViewQuestionTimeRemaining);
        relativeQuestionAlternatives = findViewById(R.id.GameActivity_RelativeQuestionAlternatives);
        relativeQuestionButton = findViewById(R.id.GameActivity_RelativeQuestionButton);
        questionTitle = findViewById(R.id.GameActivity_TextViewQuestionTitle);
        questionImage = findViewById(R.id.GameActivity_RoundedImageViewQuestionImage);
        btnConfirm = findViewById(R.id.GameActivity_ButtonConfirm);
        btnNext = findViewById(R.id.GameActivity_ButtonNext);
        recyclerViewAlternatives = findViewById(R.id.GameActivity_AlternativesRecyclerView);
    }

    private void setProperLayoutAnimation() {
        LayoutTransition layoutTransitionMain = linearLayoutMain.getLayoutTransition();
        layoutTransitionMain.enableTransitionType(LayoutTransition.CHANGING);

        LayoutTransition layoutTransitionQuestionInfo = linearLayoutQuestionInfo.getLayoutTransition();
        layoutTransitionQuestionInfo.enableTransitionType(LayoutTransition.CHANGING);

        LayoutTransition layoutTransitionQuestionAlternatives = relativeQuestionAlternatives.getLayoutTransition();
        layoutTransitionQuestionAlternatives.enableTransitionType(LayoutTransition.CHANGING);

        LayoutTransition layoutTransitionQuestionButton = relativeQuestionButton.getLayoutTransition();
        layoutTransitionQuestionButton.enableTransitionType(LayoutTransition.CHANGING);
    }

    private void setClickListeners() {
        imageViewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GameActivity.this.onBackPressed();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameInstance != null) {
                    try {
                        gameInstance.nextQuestion();
                    } catch (Exception e) {
                        Toast.makeText(GameActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void setupGame() {
        try {
            gameInstance = new Game(this, new OnGameChangeCallback() {
                @Override
                public void onGameStart() {
                    try {
                        gameInstance.nextQuestion();
                    } catch (Exception e) {
                        Toast.makeText(GameActivity.this,
                                e.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onQuestionShow(int questionNumber, int questionTotalQuantity,
                                           Question nextQuestion, IGameClientCallback gameClientCallback) {
                    showQuestion(questionNumber, questionTotalQuantity, nextQuestion);
                    if (gameClientCallback != null) {
                        // Call this if timer should be active
                        gameClientCallback.canStartQuestionTimer();
                    }
                }

                @Override
                public void onQuestionAlternativesUpdated(Question question) {
                    questionAlternativeAdapter.updateList(question.getQuestionAlternatives());
                }

                @Override
                public void onQuestionTimerTick(long timer) {
                    updateTimer(timer);
                }

                @Override
                public void onQuestionAnswered(boolean isCorrect) {
                    ((LinearLayout.LayoutParams) relativeQuestionAlternatives.getLayoutParams()).weight
                            = ((LinearLayout.LayoutParams) relativeQuestionAlternatives.getLayoutParams()).weight - 0.1f;
                    ((LinearLayout.LayoutParams) relativeQuestionButton.getLayoutParams()).weight = 0.1f;
                    btnNext.setEnabled(true);
                    btnNext.setVisibility(View.VISIBLE);
                    gameInstance.showCurrentQuestionHints(true, true);
                }

                @Override
                public void onGameEnd(boolean isWin, int correctAnswers, int incorrectAnswers,
                                      float correctAnswerPercentage) {
                    Intent intent = new Intent(GameActivity.this, EndGameActivity.class);
                    intent.putExtra(EndGameActivity.EXTRA_CORRECT_ANSWER_QUANTITY, correctAnswers);
                    intent.putExtra(EndGameActivity.EXTRA_TOTAL_QUESTION_QUANTITY, correctAnswers + incorrectAnswers);
                    intent.putExtra(EndGameActivity.EXTRA_CORRECT_ANSWER_PERCENTAGE, correctAnswerPercentage);
                    intent.putExtra(EndGameActivity.EXTRA_IS_WIN, isWin);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            gameInstance.startGame(questionQuantity);
        } catch (Exception e) {
            Toast.makeText(GameActivity.this,
                    e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void showQuestion(int questionNumber, int questionTotalQuantity, Question question) {
        if (question != null) {
            textViewQuestionNumber.setText(StringUtil.returnFormattedString(this,
                    R.string.question_number_placeholder,
                    questionNumber, questionTotalQuantity));

            if (question.getQuestionTitleId() > 0) {
                questionTitle.setVisibility(View.VISIBLE);
                questionTitle.setText(StringUtil.returnFormattedString(this, question.getQuestionTitleId()));
                // Fade in animation
                Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
                fadeIn.setDuration(700);
                fadeIn.setFillAfter(true);
                questionTitle.setAnimation(fadeIn);
            } else {
                // Fade out animation
                Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
                fadeOut.setDuration(700);
                fadeOut.setFillAfter(true);
                questionTitle.setAnimation(fadeOut);
                questionTitle.setText(null);
                questionTitle.setVisibility(View.GONE);
            }

            if (question.getQuestionDrawableId() > 0) {
                ((LinearLayout.LayoutParams) linearLayoutQuestionInfo.getLayoutParams()).weight = 0.35f;
                ((LinearLayout.LayoutParams) relativeQuestionAlternatives.getLayoutParams()).weight = 0.65f;
                questionImage.setImageDrawable(getResources().getDrawable(question.getQuestionDrawableId()));
                questionImage.setVisibility(View.VISIBLE);
                // Fade in animation
                Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
                fadeIn.setDuration(700);
                fadeIn.setFillAfter(true);
                questionImage.setAnimation(fadeIn);
            } else {
                // Fade out animation
                Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
                fadeOut.setDuration(700);
                fadeOut.setFillAfter(true);
                questionImage.setAnimation(fadeOut);
                ((LinearLayout.LayoutParams) linearLayoutQuestionInfo.getLayoutParams()).weight = 0.18f;
                ((LinearLayout.LayoutParams) relativeQuestionAlternatives.getLayoutParams()).weight = 0.82f;
                questionImage.setImageDrawable(null);
                questionImage.setVisibility(View.GONE);
            }
            showQuestionAlternatives(question);
        }
    }

    private void showQuestionAlternatives(Question question) {
        if (question != null) {
            switch (question.getViewMode()) {
                case QuestionAlternativeViewMode.GRID:
                    int columnQty = ViewUtil.calculateNoOfColumns(this,
                            QuestionAlternativeViewMode.GRID_DEFAULT_COLUMN_WIDTH_DP);
                    StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(columnQty, StaggeredGridLayoutManager.VERTICAL);
                    staggeredGridLayoutManager.setGapStrategy(
                            StaggeredGridLayoutManager.GAP_HANDLING_NONE);
                    recyclerViewAlternatives.setHasFixedSize(true);
                    recyclerViewAlternatives.setLayoutManager(staggeredGridLayoutManager);
                    if (recyclerViewAlternatives.getItemDecorationCount() <= 0) {
                        int spacing = 30;
                        boolean includeEdge = false;
                        recyclerViewAlternatives.addItemDecoration(new GridSpacingItemDecoration(columnQty, spacing, includeEdge));
                    }
                    break;
                case QuestionAlternativeViewMode.LIST:
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                    recyclerViewAlternatives.setHasFixedSize(true);
                    recyclerViewAlternatives.setLayoutManager(linearLayoutManager);
                    break;
            }

            questionAlternativeAdapter = new QuestionAlternativeAdapter(this, question, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    QuestionAlternative alternative = (QuestionAlternative) view.getTag();
                    try {
                        gameInstance.answerQuestion(alternative.getId());
                    } catch (Exception e) {
                        Toast.makeText(GameActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, true);

            AnimationSet set = new AnimationSet(true);

            // Fade in animation
            Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
            fadeIn.setDuration(750);
            fadeIn.setFillAfter(true);
            set.addAnimation(fadeIn);

            int height = this.getResources().getDisplayMetrics().heightPixels;
            // Slide up animation from bottom of screen
            Animation slideUp = new TranslateAnimation(0, 0, height, 0);
            slideUp.setInterpolator(new DecelerateInterpolator(4.f));
            slideUp.setDuration(750);
            set.addAnimation(slideUp);

            // Set up the animation controller              (second parameter is the delay)
            LayoutAnimationController controller = new LayoutAnimationController(set, 0.2f);
            recyclerViewAlternatives.setLayoutAnimation(controller);
            recyclerViewAlternatives.setAdapter(questionAlternativeAdapter);
        }
    }

    private void updateTimer(long timeRemaining) {
        if (cardViewQuestionTimer.getVisibility() == View.GONE) {
            cardViewQuestionTimer.setVisibility(View.VISIBLE);
        }
        textViewQuestionTimer.setText(StringUtil.returnFormattedString(this,
                R.string.question_timer_placeholder,
                timeRemaining));
    }
}