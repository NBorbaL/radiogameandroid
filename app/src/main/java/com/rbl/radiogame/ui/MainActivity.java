package com.rbl.radiogame.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.rbl.radiogame.R;
import com.rbl.radiogame.model.Game;

public class MainActivity extends AppCompatActivity {

    private Button btnPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewsById();
        setClickListeners();
    }

    private void findViewsById() {
        btnPlay = findViewById(R.id.MainActivity_ButtonPlay);
    }

    private void setClickListeners() {
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playGame(null);
            }
        });
    }

    private void playGame(Integer questionQuantity) {
        Intent intent = new Intent(MainActivity.this, GameActivity.class);
        if (questionQuantity != null && questionQuantity > 0) {
            intent.putExtra(getString(GameActivity.EXTRA_QUESTION_QUANTITY), questionQuantity);
        }
        startActivity(intent);
    }
}