package com.rbl.radiogame.interfaces;


import com.rbl.radiogame.model.Question;

public interface OnGameChangeCallback {
    void onGameStart();

    void onQuestionShow(int questionNumber, int questionTotalQuantity,
                        Question nextQuestion, IGameClientCallback gameClientCallback);

    void onQuestionAlternativesUpdated(Question question);

    void onQuestionTimerTick(long timer);

    void onQuestionAnswered(boolean isCorrect);

    void onGameEnd(boolean isWin, int correctAnswers, int wrongAnswers, float correctAnswerPercentage);
}
