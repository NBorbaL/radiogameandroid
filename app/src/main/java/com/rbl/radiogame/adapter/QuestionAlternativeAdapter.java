package com.rbl.radiogame.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rbl.radiogame.R;
import com.rbl.radiogame.model.Question;
import com.rbl.radiogame.model.QuestionAlternative;
import com.rbl.radiogame.model.enums.QuestionAlternativeViewMode;
import com.rbl.radiogame.util.ImageUtil;

import java.util.List;

/**
 * Criado por renan.lucas em 03/04/2018.
 */

public class QuestionAlternativeAdapter extends RecyclerView.Adapter<QuestionAlternativeAdapter.ViewHolder> {

    private Context ctx;
    private Question question;
    private View.OnClickListener cardClickListener;
    private boolean compressBitmaps;

    private static final int FADE_ANIMATION_DURATION = 500;

    public QuestionAlternativeAdapter(Context ctx, Question question,
                                      View.OnClickListener cardClickListener, boolean compressBitmaps) {
        this.ctx = ctx;
        this.question = question;
        this.cardClickListener = cardClickListener;
        this.compressBitmaps = compressBitmaps;
        //question.sortAlternativesCorrectly(ctx);
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final ViewHolder viewHolder;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_alternative, parent, false);
        viewHolder = new ViewHolder(view);
        if (cardClickListener != null) {
            view.setOnClickListener(cardClickListener);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final CardView cardViewMain = holder.cardViewMain;
        final TextView textViewAlternativeLetter = holder.textViewAlternativeLetter;
        final RoundedImageView roundedImageView = holder.roundedImageView;
        final TextView textViewDescription = holder.textViewDescription;
        final RelativeLayout relativeLayoutHint = holder.relativeLayoutHint;
        final TextView textViewHint = holder.textViewHint;

        final QuestionAlternative alternative = question.getQuestionAlternatives().get(position);
        holder.itemView.setTag(alternative);

        if (question.getViewMode() == QuestionAlternativeViewMode.GRID) {
            // Sets proper animation to card
            // Without this, animateLayoutChanges does not work!
            cardViewMain.getLayoutTransition()
                    .enableTransitionType(LayoutTransition.CHANGING);
        }

        // Disables card view if alternative was answered already
        cardViewMain.setClickable(alternative.isClickable());

        if (alternative.getAlternativeLetterId() > 0) {
            textViewAlternativeLetter.setText(ctx.getString(alternative.getAlternativeLetterId()));
            if (textViewAlternativeLetter.getVisibility() != View.VISIBLE) {
                textViewAlternativeLetter.setVisibility(View.VISIBLE);
                // Fade in animation
                Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
                fadeIn.setDuration(FADE_ANIMATION_DURATION);
                fadeIn.setFillAfter(true);
                textViewAlternativeLetter.startAnimation(fadeIn);
            }
        } else {
            if (textViewAlternativeLetter.getVisibility() != View.GONE) {
                // Fade out animation
                Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
                fadeOut.setDuration(FADE_ANIMATION_DURATION);
                fadeOut.setFillAfter(true);
                fadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        textViewAlternativeLetter.setText(null);
                        textViewAlternativeLetter.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                textViewAlternativeLetter.startAnimation(fadeOut);
            }
        }

        if (alternative.getAlternativeDrawableId() > 0) {
            roundedImageView.setVisibility(View.VISIBLE);
            if (compressBitmaps) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final Bitmap bmp;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            bmp = ImageUtil.resize(ImageUtil.drawableToBitmap(ctx.getDrawable(alternative.getAlternativeDrawableId())),
                                    roundedImageView.getLayoutParams().width * 2,
                                    roundedImageView.getLayoutParams().height * 2);
                        } else {
                            bmp = ImageUtil.resize(ImageUtil.drawableToBitmap(ctx.getResources().getDrawable(alternative.getAlternativeDrawableId())),
                                    roundedImageView.getLayoutParams().width * 2,
                                    roundedImageView.getLayoutParams().height * 2);
                        }
                        fillImageView(false, roundedImageView, bmp, 0);
                    }
                }).start();
            } else {
                fillImageView(false, roundedImageView, null, alternative.getAlternativeDrawableId());
            }
        } else {
            roundedImageView.setImageDrawable(null);
            roundedImageView.setVisibility(View.GONE);
        }

        if (alternative.isShouldShowDescription()
                && alternative.getAlternativeDescriptionId() > 0) {
            textViewDescription.setText(ctx.getText(alternative.getAlternativeDescriptionId()));
            if (textViewDescription.getVisibility() != View.VISIBLE) {
                textViewDescription.setVisibility(View.VISIBLE);
                // Fade in animation
                Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
                fadeIn.setDuration(FADE_ANIMATION_DURATION);
                fadeIn.setFillAfter(true);
                textViewDescription.startAnimation(fadeIn);
            }
        } else {
            if (textViewDescription.getVisibility() != View.GONE) {
                // Fade out animation
                Animation fadeOut = new AlphaAnimation(1.0f, 0.0f);
                fadeOut.setDuration(FADE_ANIMATION_DURATION);
                fadeOut.setFillAfter(true);
                fadeOut.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        textViewDescription.setText(null);
                        textViewDescription.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                textViewDescription.startAnimation(fadeOut);
            }
        }

        if (alternative.isShouldShowHint()
                && alternative.getAlternativeHintId() > 0) {
            String hintText = ctx.getString(alternative.getAlternativeHintId());
            if (alternative.isUserAnswer()) {
                hintText = ctx.getString(R.string.your_answer) + "\n" + hintText;
            }

            textViewHint.setText(hintText);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                relativeLayoutHint.setBackgroundColor(ctx.getColor(alternative.getAlternativeHintBackgroundColorId()));
            } else {
                relativeLayoutHint.setBackgroundColor(ctx.getResources().getColor(alternative.getAlternativeHintBackgroundColorId()));
            }

            Animation translateAnim = AnimationUtils.loadAnimation(ctx,
                    R.anim.slide_up);
            translateAnim.setFillAfter(true);
            translateAnim.setFillEnabled(true);
            translateAnim.setFillBefore(false);
            translateAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    textViewHint.setVisibility(View.VISIBLE);
                    relativeLayoutHint.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            relativeLayoutHint.startAnimation(translateAnim);
        } else {
            if (relativeLayoutHint.getVisibility() == View.VISIBLE) {
                Animation translateAnim = AnimationUtils.loadAnimation(ctx,
                        R.anim.slide_down);
                translateAnim.setFillAfter(true);
                translateAnim.setFillEnabled(true);
                translateAnim.setFillBefore(false);
                translateAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        textViewHint.setText(null);
                        textViewHint.setVisibility(View.GONE);
                        relativeLayoutHint.setVisibility(View.GONE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            relativeLayoutHint.setBackgroundColor(ctx.getColor(alternative.getAlternativeHintBackgroundColorId()));
                        } else {
                            relativeLayoutHint.setBackgroundColor(ctx.getResources().getColor(alternative.getAlternativeHintBackgroundColorId()));
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                relativeLayoutHint.startAnimation(translateAnim);
            } else {
                textViewHint.setText(null);
                textViewHint.setVisibility(View.GONE);
                relativeLayoutHint.setVisibility(View.GONE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    relativeLayoutHint.setBackgroundColor(ctx.getColor(alternative.getAlternativeHintBackgroundColorId()));
                } else {
                    relativeLayoutHint.setBackgroundColor(ctx.getResources().getColor(alternative.getAlternativeHintBackgroundColorId()));
                }
            }
        }
    }

    private void fillImageView(final boolean animate, final ImageView imageView,
                               final Bitmap bmp, final int drawableId) {
        if (drawableId > 0 || bmp != null) {
            Activity activity = (Activity) ctx;
            if (animate) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                final ObjectAnimator oa1 = ObjectAnimator.ofFloat(imageView, "scaleX", 1f, 0f);
                                final ObjectAnimator oa2 = ObjectAnimator.ofFloat(imageView, "scaleX", 0f, 1f);
                                oa1.setInterpolator(new DecelerateInterpolator());
                                oa2.setInterpolator(new AccelerateDecelerateInterpolator());
                                oa1.addListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                                        if (drawableId > 0) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                imageView.setImageDrawable(ctx.getDrawable(drawableId));
                                                imageView.setVisibility(View.VISIBLE);
                                            } else {
                                                imageView.setImageDrawable(ctx.getResources().getDrawable(drawableId));
                                            }
                                        } else {
                                            imageView.setImageBitmap(bmp);
                                        }
                                        oa2.start();
                                    }
                                });
                                oa1.start();
                            }
                        }, 100);
                    }
                });
            } else {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        if (drawableId > 0) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                imageView.setImageDrawable(ctx.getDrawable(drawableId));
                            } else {
                                imageView.setImageDrawable(ctx.getResources().getDrawable(drawableId));
                            }
                        } else {
                            imageView.setImageBitmap(bmp);
                        }
                    }
                });
            }
        }
    }

    public void updateList(List<QuestionAlternative> newList) {
        question.setQuestionAlternatives(newList);
        //question.sortAlternativesCorrectly(ctx);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return question.getQuestionAlternatives().size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardViewMain;
        TextView textViewAlternativeLetter;
        RoundedImageView roundedImageView;
        TextView textViewDescription;
        RelativeLayout relativeLayoutHint;
        TextView textViewHint;

        private ViewHolder(View itemView) {
            super(itemView);
            this.cardViewMain = itemView.findViewById(R.id.CardAlternative_CardViewMain);
            this.textViewAlternativeLetter = itemView.findViewById(R.id.CardAlternative_TextViewAlternativeLetter);
            this.roundedImageView = itemView.findViewById(R.id.CardAlternative_RoundedImageView);
            this.textViewDescription = itemView.findViewById(R.id.CardAlternative_TextViewDescription);
            this.relativeLayoutHint = itemView.findViewById(R.id.CardAlternative_RelativeHint);
            this.textViewHint = itemView.findViewById(R.id.CardAlternative_TextViewHint);
        }
    }
}