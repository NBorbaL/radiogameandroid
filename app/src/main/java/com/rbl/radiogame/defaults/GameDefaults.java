package com.rbl.radiogame.defaults;

import com.rbl.radiogame.R;

public class GameDefaults {
    public static final boolean DEFAULT_SHOULD_SHUFFLE_QUESTIONS = false;
    public static final boolean DEFAULT_SHOULD_SHUFFLE_ALTERNATIVES = false;
    public static final boolean DEFAULT_IS_TIMER_ACTIVE = false;

    public static final int DEFAULT_QUESTION_QUANTITY = 2;
    public static final long DEFAULT_QUESTION_TIME_MILLIS = 30000;
    public static final long DEFAULT_QUESTION_WRONG_AWSWER_HINT_TIME_MILLIS = 10000;
    public static final long DEFAULT_QUESTION_RIGHT_AWSWER_HINT_TIME_MILLIS = 3000;

    public static final int DEFAULT_SUCCESS_HINT_BACKGROUND_COLOR_ID = R.color.yellow;
    public static final int DEFAULT_ERROR_HINT_BACKGROUND_COLOR_ID = R.color.red_incorrect;

    public static final int DEFAULT_PERCENT_FOR_WIN = 50;
}
