package com.rbl.radiogame.dal;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rbl.radiogame.R;
import com.rbl.radiogame.defaults.GameDefaults;
import com.rbl.radiogame.model.Question;
import com.rbl.radiogame.model.QuestionAlternative;
import com.rbl.radiogame.model.json.JsonAlternative;
import com.rbl.radiogame.model.json.JsonQuestion;
import com.rbl.radiogame.model.json.JsonQuestionFile;
import com.rbl.radiogame.util.AssetsUtil;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuestionDatabase {

    private List<Question> gameQuestions = new ArrayList<>();
    private Context ctx;

    public QuestionDatabase(Context ctx) {
        this.ctx = ctx;
    }

    /*private void generateList() {
        gameQuestions.clear();
        Question questionOne = new Question(R.string.question_one_title,
                0,
                QuestionAlternativeViewMode.GRID);
        questionOne.addQuestionAlternative(new QuestionAlternative(0,
                R.string.question_one_alternative_a_description,
                R.drawable.question_one_a,
                R.string.wrong_alternative,
                false));
        questionOne.addQuestionAlternative(new QuestionAlternative(0,
                R.string.question_one_alternative_b_description,
                R.drawable.question_one_b,
                R.string.wrong_alternative,
                false));
        questionOne.addQuestionAlternative(new QuestionAlternative(0,
                R.string.question_one_alternative_c_description,
                R.drawable.question_one_c,
                R.string.wrong_alternative,
                false));
        QuestionAlternative correctAlternativeOne = new QuestionAlternative(0,
                R.string.question_one_alternative_d_description,
                R.drawable.question_one_d,
                R.string.correct_alternative,
                false);
        correctAlternativeOne.setAlternativeHintBackgroundColorId(GameDefaults.DEFAULT_SUCCESS_HINT_BACKGROUND_COLOR_ID);
        questionOne.addQuestionAlternative(correctAlternativeOne);
        questionOne.setCorrectAlternative(correctAlternativeOne);

        questionOne.addQuestionAlternative(new QuestionAlternative(0,
                R.string.question_one_alternative_e_description,
                R.drawable.question_one_e,
                R.string.wrong_alternative,
                false));
        gameQuestions.add(questionOne);
    }*/

    private void generateList(Context ctx) {
        gameQuestions.clear();
        String questionJsonString = AssetsUtil.getJsonFromAssets(ctx, "questions.json");
        Type listUserType = new TypeToken<JsonQuestionFile>() {
        }.getType();
        gameQuestions = convertJsonModelToQuestionModel(ctx,
                (JsonQuestionFile) new Gson().fromJson(questionJsonString, listUserType));
    }

    private List<Question> convertJsonModelToQuestionModel(Context ctx, JsonQuestionFile jsonQuestions) {
        List<Question> questions = new ArrayList<>();
        if (jsonQuestions != null && jsonQuestions.getQuestions() != null) {
            for (JsonQuestion jsonQuestion : jsonQuestions.getQuestions()) {
                if (jsonQuestion.getAlternatives() != null) {
                    String titleIdentifier = jsonQuestion.getTitle() + "_title";
                    int drawableId = 0;
                    if (jsonQuestion.getDrawable() != null && !jsonQuestion.getDrawable().isEmpty()) {
                        String drawableIdentifier = jsonQuestion.getTitle() + "_image";
                        drawableId = ctx.getResources().getIdentifier(drawableIdentifier, "drawable", ctx.getPackageName());
                    }

                    Question question = new Question(
                            ctx.getResources().getIdentifier(titleIdentifier, "string", ctx.getPackageName()),
                            drawableId,
                            jsonQuestion.getViewMode());

                    if (GameDefaults.DEFAULT_SHOULD_SHUFFLE_ALTERNATIVES) {
                        Collections.shuffle(jsonQuestion.getAlternatives());
                    }
                    int alternativeCount = 0;
                    for (JsonAlternative jsonAlternative : jsonQuestion.getAlternatives()) {
                        alternativeCount++;
                        String alternativeLetterIdentifier = "alternative_" + alternativeCount;
                        String descriptionIdentifier = jsonQuestion.getTitle() + "_"
                                + jsonAlternative.getTitle()
                                + "_description";
                        int alternativeDrawableId;
                        String drawableIdentifier = jsonQuestion.getTitle() + "_" + jsonAlternative.getDrawable();
                        alternativeDrawableId = ctx.getResources().getIdentifier(drawableIdentifier, "drawable", ctx.getPackageName());

                        int hintId = R.string.wrong_alternative;
                        if (jsonAlternative.isCorrect()) {
                            hintId = R.string.correct_alternative;
                        }

                        QuestionAlternative questionAlternative = new QuestionAlternative(
                                ctx.getResources().getIdentifier(alternativeLetterIdentifier, "string", ctx.getPackageName()),
                                ctx.getResources().getIdentifier(descriptionIdentifier, "string", ctx.getPackageName()),
                                alternativeDrawableId,
                                hintId,
                                jsonAlternative.getShouldShowDescription()
                        );
                        if (jsonAlternative.isCorrect()) {
                            questionAlternative.setAlternativeHintBackgroundColorId(GameDefaults.DEFAULT_SUCCESS_HINT_BACKGROUND_COLOR_ID);
                            question.setCorrectAlternative(questionAlternative);
                        } else {
                            questionAlternative.setAlternativeHintBackgroundColorId(GameDefaults.DEFAULT_ERROR_HINT_BACKGROUND_COLOR_ID);
                        }
                        question.addQuestionAlternative(questionAlternative);
                    }
                    if (question.isValidQuestion()) {
                        questions.add(question);
                    }
                }
            }
        }
        return questions;
    }

    public List<Question> getAllGameQuestions() {
        generateList(ctx);
        return gameQuestions;
    }

    public List<Question> getGameQuestions(int questionQuantity) {
        generateList(ctx);
        if (gameQuestions != null) {
            if (GameDefaults.DEFAULT_SHOULD_SHUFFLE_QUESTIONS) {
                Collections.shuffle(gameQuestions);
            }
            while (gameQuestions.size() > questionQuantity) {
                gameQuestions.remove(0);
            }
        }
        return gameQuestions;
    }
}
